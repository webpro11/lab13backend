import { IsNotEmpty, Length, Min } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @Length(3)
  name: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  @Min(3)
  age: number;

  @IsNotEmpty()
  @Length(1, 8)
  gender: number;
}
